package com.gspann.dataprocessor.businessobjects;

public class InventoryItem
{
    private String ItemID;

    private String UnitOfMeasure;

    private String ProductClass;

    private AvailabilityChanges AvailabilityChanges;

    public String getItemID ()
    {
        return ItemID;
    }

    public void setItemID (String ItemID)
    {
        this.ItemID = ItemID;
    }

    public String getUnitOfMeasure ()
    {
        return UnitOfMeasure;
    }

    public void setUnitOfMeasure (String UnitOfMeasure)
    {
        this.UnitOfMeasure = UnitOfMeasure;
    }

    public String getProductClass ()
    {
        return ProductClass;
    }

    public void setProductClass (String ProductClass)
    {
        this.ProductClass = ProductClass;
    }

    public AvailabilityChanges getAvailabilityChanges ()
    {
        return AvailabilityChanges;
    }

    public void setAvailabilityChanges (AvailabilityChanges AvailabilityChanges)
    {
        this.AvailabilityChanges = AvailabilityChanges;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ItemID = "+ItemID+", UnitOfMeasure = "+UnitOfMeasure+", ProductClass = "+ProductClass+", AvailabilityChanges = "+AvailabilityChanges+"]";
    }
}