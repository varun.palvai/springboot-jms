package com.gspann.dataprocessor.businessobjects;

import java.util.List;

public class AvailabilityChanges {
	private AvailabilityChange AvailabilityChange;

	public AvailabilityChange getAvailabilityChange() {
		return AvailabilityChange;
	}

	public void setAvailabilityChange(AvailabilityChange AvailabilityChange) {
		this.AvailabilityChange = AvailabilityChange;
	}

	@Override
	public String toString() {
		return "ClassPojo [AvailabilityChange = " + AvailabilityChange + "]";
	}
}