package com.gspann.dataprocessor.businessobjects;

public class AvailabilityChange {
	private String OnhandAvailableQuantity;

	private String AlertType;

	private String FirstFutureAvailableDate;

	private String Node;

	private String OnhandAvailableDate;

	private String AlertLevel;

	private String FutureAvailableQuantity;

	private String AlertQuantity;

	private String AlertRaisedOn;

	private String AgentCriteriaId;

	private String FutureAvailableDate;

	private String MonitorOption;

	public String getOnhandAvailableQuantity() {
		return OnhandAvailableQuantity;
	}

	public void setOnhandAvailableQuantity(String OnhandAvailableQuantity) {
		this.OnhandAvailableQuantity = OnhandAvailableQuantity;
	}

	public String getAlertType() {
		return AlertType;
	}

	public void setAlertType(String AlertType) {
		this.AlertType = AlertType;
	}

	public String getFirstFutureAvailableDate() {
		return FirstFutureAvailableDate;
	}

	public void setFirstFutureAvailableDate(String FirstFutureAvailableDate) {
		this.FirstFutureAvailableDate = FirstFutureAvailableDate;
	}

	public String getNode() {
		return Node;
	}

	public void setNode(String Node) {
		this.Node = Node;
	}

	public String getOnhandAvailableDate() {
		return OnhandAvailableDate;
	}

	public void setOnhandAvailableDate(String OnhandAvailableDate) {
		this.OnhandAvailableDate = OnhandAvailableDate;
	}

	public String getAlertLevel() {
		return AlertLevel;
	}

	public void setAlertLevel(String AlertLevel) {
		this.AlertLevel = AlertLevel;
	}

	public String getFutureAvailableQuantity() {
		return FutureAvailableQuantity;
	}

	public void setFutureAvailableQuantity(String FutureAvailableQuantity) {
		this.FutureAvailableQuantity = FutureAvailableQuantity;
	}

	public String getAlertQuantity() {
		return AlertQuantity;
	}

	public void setAlertQuantity(String AlertQuantity) {
		this.AlertQuantity = AlertQuantity;
	}

	public String getAlertRaisedOn() {
		return AlertRaisedOn;
	}

	public void setAlertRaisedOn(String AlertRaisedOn) {
		this.AlertRaisedOn = AlertRaisedOn;
	}

	public String getAgentCriteriaId() {
		return AgentCriteriaId;
	}

	public void setAgentCriteriaId(String AgentCriteriaId) {
		this.AgentCriteriaId = AgentCriteriaId;
	}

	public String getFutureAvailableDate() {
		return FutureAvailableDate;
	}

	public void setFutureAvailableDate(String FutureAvailableDate) {
		this.FutureAvailableDate = FutureAvailableDate;
	}

	public String getMonitorOption() {
		return MonitorOption;
	}

	public void setMonitorOption(String MonitorOption) {
		this.MonitorOption = MonitorOption;
	}

	@Override
	public String toString() {
		return "ClassPojo [OnhandAvailableQuantity = " + OnhandAvailableQuantity + ", AlertType = " + AlertType
				+ ", FirstFutureAvailableDate = " + FirstFutureAvailableDate + ", Node = " + Node
				+ ", OnhandAvailableDate = " + OnhandAvailableDate + ", AlertLevel = " + AlertLevel
				+ ", FutureAvailableQuantity = " + FutureAvailableQuantity + ", AlertQuantity = " + AlertQuantity
				+ ", AlertRaisedOn = " + AlertRaisedOn + ", AgentCriteriaId = " + AgentCriteriaId
				+ ", FutureAvailableDate = " + FutureAvailableDate + ", MonitorOption = " + MonitorOption + "]";
	}
}
