//package com.gspann.mqprocessor;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.jms.core.JmsOperations;
//import org.springframework.stereotype.Component;
//
//import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;
//import com.gspann.mqprocessor.service.IInventoryItemService;
//
////@Component
//public class InventoryItemReceiver {
//
//	/*
//	 * @JmsListener(destination = "STL.RTAM.MG", containerFactory =
//	 * "mqQueueConnectionFactory") public void receiveMessage(InventoryItemReceived
//	 * inventoryItem) { System.out.println("following message is received:" +
//	 * inventoryItem.toString()); // transactionRepository.save(transaction); }
//	 */
//
//	@Autowired
//	private IInventoryItemService iInventoryItemService;
//
//	@Autowired
//	JmsOperations jmsOperations;
//
//	public void receive() {
//		InventoryItemReceived inventoryItem = (InventoryItemReceived) jmsOperations.receive("STL.RTAM.MG");
//		System.out.println("following message is received:" + inventoryItem.toString());
//		//iInventoryItemService.processMessage(inventoryItem);
//	}
//}
