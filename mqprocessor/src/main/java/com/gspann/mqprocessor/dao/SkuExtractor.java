package com.gspann.mqprocessor.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class SkuExtractor implements ResultSetExtractor<Map<String, String>>{

	@Override
	public Map<String, String> extractData(ResultSet rs) throws SQLException, DataAccessException {
		Map<String, String> skuMap = new HashMap<String, String>();
		while(rs.next()) {
			String rowId = rs.getString("row_id");
			String sku = rs.getString("sku");
			skuMap.put(rowId, sku);
		}
		return skuMap;
	}

}
