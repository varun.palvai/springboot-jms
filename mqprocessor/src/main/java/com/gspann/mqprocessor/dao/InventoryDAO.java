package com.gspann.mqprocessor.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class InventoryDAO implements IInventoryDAO {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static String CATALOG_INVENTORY_STOCK_ITEM = "UPDATE cataloginventory_stock_item SET qty=?,is_in_stock=? WHERE product_id= ?";

	public static String CATALOG_INVENTORY_STOCK_STATUS = "UPDATE cataloginventory_stock_status SET qty=?,stock_status=? WHERE product_id= ?";

	public static String CATALOG_INVENTORY_STOCK_STATUS_IDX = "UPDATE cataloginventory_stock_status_idx SET qty=?,stock_status=? WHERE product_id= ?";

	@Override
	public Map<String, String> getSkuMap() {
		String sql = "SELECT row_id,sku FROM catalog_product_entity";
		Map<String, String> skuMap = this.jdbcTemplate.query(sql, new SkuExtractor());
		return skuMap;
	}

	@Override
	public void updateStocks(String productId, int quantityValue, String stockStatus) {
		jdbcTemplate.update(CATALOG_INVENTORY_STOCK_ITEM, quantityValue, stockStatus, productId);
		jdbcTemplate.update(CATALOG_INVENTORY_STOCK_STATUS, quantityValue, stockStatus, productId);
		jdbcTemplate.update(CATALOG_INVENTORY_STOCK_STATUS_IDX, quantityValue, stockStatus, productId);
	}

}
