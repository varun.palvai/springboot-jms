package com.gspann.mqprocessor.dao;

import java.util.Map;

public interface IInventoryDAO {
	
	Map<String,String> getSkuMap();

	void updateStocks(String productId, int quantityValue, String stockStatus);
   
}
 