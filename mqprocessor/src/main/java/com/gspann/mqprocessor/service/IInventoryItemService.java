package com.gspann.mqprocessor.service;

import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;

public interface IInventoryItemService {

	void processMessage(InventoryItemReceived inventoryItem);
	//TODO : remove below and make it private in service
    void populateCatalogEntities();
}
