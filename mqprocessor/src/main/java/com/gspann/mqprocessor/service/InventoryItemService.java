package com.gspann.mqprocessor.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;
import com.gspann.mqprocessor.App;
import com.gspann.mqprocessor.dao.IInventoryDAO;

@Service
public class InventoryItemService implements IInventoryItemService {

	@Autowired
	private IInventoryDAO inventoryDAO;
	
	private static Logger log = LoggerFactory.getLogger(InventoryItemService.class);

	Map<String, String> skuMap = new HashMap<String, String>();

	@Override
	public void processMessage(InventoryItemReceived inventoryItem) {
		populateCatalogEntities();

		if (null == inventoryItem.getInventoryItem().getItemID()
				|| inventoryItem.getInventoryItem().getItemID().length() <= 0)
			return;

		String quantity = inventoryItem.getInventoryItem().getAvailabilityChanges().getAvailabilityChange()
				.getOnhandAvailableQuantity();

		String sku = inventoryItem.getInventoryItem().getItemID();

		String productId = skuMap.get(sku);

		if (null == productId || productId.length() <= 0)
			return;

		int quantityValue = Integer.parseInt(quantity);
		String stockStatus = "1";
		if (quantityValue <= 0) {
			quantityValue = 0;
			stockStatus = "0";
		}
		
		inventoryDAO.updateStocks(productId, quantityValue, stockStatus);

	}

	public void populateCatalogEntities() {
		if (skuMap.keySet().size() > 0) {
			log.info("SKU MAP already populated, SIZE = "+skuMap.size());
			return;
		}
		skuMap = inventoryDAO.getSkuMap();
		System.out.println("SKU MAP SIZE = "+skuMap.size());
		
	}

}
