package com.gspann.mqprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gspann.mqprocessor.service.IInventoryItemService;
import com.gspann.mqprocessor.service.InventoryItemService;

@RestController
public class HomePage {
	
	private static Logger log = LoggerFactory.getLogger(HomePage.class);

	@Autowired
	IInventoryItemService is;
	
	@RequestMapping("/")
	String home() {
		log.info("Home page called");
		is.populateCatalogEntities();
		return "Hello World+!";
	}
}
