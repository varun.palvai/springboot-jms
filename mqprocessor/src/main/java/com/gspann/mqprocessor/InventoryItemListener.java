package com.gspann.mqprocessor;

import javax.jms.JMSException;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Component;

import com.gspann.dataprocessor.businessobjects.InventoryItemReceived;
import com.gspann.mqprocessor.service.IInventoryItemService;

@Component
public class InventoryItemListener {

	@Autowired
	private IInventoryItemService iInventoryItemService;
	
	private static Logger log = LoggerFactory.getLogger(InventoryItemListener.class);

	@JmsListener(destination = "STL.RTAM2.MG", containerFactory = "jmsListenerContainerFactory")
	public String receiveMessage(final InventoryItemReceived inventoryItem) throws JMSException {
		log.info("Received message " + inventoryItem.getInventoryItem().toString());
		// iInventoryItemService.processMessage(inventoryItem);
		return null;
	}
}
