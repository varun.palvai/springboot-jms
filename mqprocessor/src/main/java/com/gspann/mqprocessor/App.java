package com.gspann.mqprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

import com.gspann.mqprocessor.service.IInventoryItemService;

@SpringBootApplication
public class App extends SpringBootServletInitializer{

	//@Autowired
	//JmsOperations jmsOperations;

	private static Logger log = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(App.class, args);
		log.info("MQProcessor started");
		// Arrays.asList(context.getBeanDefinitionNames()).stream().sorted().forEach(System.out::println);
		//InventoryItemReceiver mr = context.getBean(InventoryItemReceiver.class);
		IInventoryItemService ms = context.getBean(IInventoryItemService.class);
		ms.populateCatalogEntities();
		//mr.receive();
	}

}
